import sys
import feedparser
from flask import Flask
from flask import render_template

reload(sys)
sys.setdefaultencoding('UTF8')

app = Flask(__name__)
THE_ECONOMIST = "http://www.economist.com/sections/economics/rss.xml"

@app.route("/")
def home():

    feed = feedparser.parse(THE_ECONOMIST) 

    #first_article = feed['entries']
    articles = []
    for item in feed['entries']:
        articles.append({
            "title" : item.title, #.get("title"),
            "published" : item.published, #get("published"),
            "summary" : item.get("description")
        })

    return render_template("home.html", articles = articles)


@app.route("/ecominist")
def get_news():
    feed = feedparser.parse(THE_ECONOMIST) 

    first_article = feed['entries'][0]
    body = ""

    for item in feed['entries']:
        body += """ 
        <div>
                <h1>The Economist</h1>
                <b>{0}</b></br>
                <i>{1}</i></br>
                <p>{2}</>
        </div>
        """.format(item.get("title"), item.get("published"), item.get("description"))

    
    
    return """ 
        <html>
            <body>
            """+body+"""
            </body>
        </html>
    """









if __name__ == '__main__':
    app.run(port=5000, debug=True)